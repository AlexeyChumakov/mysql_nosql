﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace MySQL___NoSQL
{
	class TestConnection
	{
		private readonly string connectionString;

		public TestConnection()
		{
			connectionString = "server=localhost;user id=root;password=Enable123;database=RailwayDB";
		}

		public void GetAllRoutes()
		{
			Console.WriteLine("Все маршуты поездов: \n");
			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				string sql = "SELECT `id_route`, `point_of_departure`,`point_of_arrival`, `time_of_arrival`, `time_of_departure` FROM `Route`";

				MySqlCommand cmd = new MySqlCommand(sql, con);
				con.Open();
				MySqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					Console.WriteLine($"[{rdr["id_route"]}, {rdr["point_of_departure"]}, {rdr["point_of_arrival"]}, {rdr["time_of_departure"]}, {rdr["time_of_arrival"]}]");
				}
			}
		}

		public void GetRouteById()
		{
			Console.Write("Введите id  маршрута, который вы хотите найти : ");
			int id = int.Parse(Console.ReadLine());

			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				string sql = "SELECT `id_route`, `point_of_departure`,`point_of_arrival`, `time_of_arrival`, `time_of_departure` FROM `Route` WHERE `id_route` = @id_route; ";

				MySqlCommand cmd = new MySqlCommand(sql, con);
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", id);
				MySqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					Console.WriteLine($"[{rdr["id_route"]}, {rdr["point_of_departure"]}, {rdr["point_of_arrival"]}, {rdr["time_of_departure"]}, {rdr["time_of_arrival"]}]");
				}
			}
		}
		
		public void АddRoute()
		{
			Console.WriteLine("Добавление маршрута: \n");

			Console.Write("Введите id маршрута: ");
			int id_route = int.Parse(Console.ReadLine());
			Console.Write("Введите станцию отправления: ");
			string point_of_departure = Console.ReadLine();
			Console.Write("Введите станцию прибытия: ");
			string point_of_arrival = Console.ReadLine();
			Console.Write("Введите время отправления: ");
			DateTime time_of_departure = DateTime.Parse(Console.ReadLine());
			Console.Write("Введите время прибытия: ");
			DateTime time_of_arrival = DateTime.Parse(Console.ReadLine());


			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				string sql = "INSERT INTO `Route` (`id_route`, `point_of_departure`, `point_of_arrival`, `time_of_arrival`, `time_of_departure`) VALUES (@id_route, @point_of_departure, @point_of_arrival, @time_of_arrival, @time_of_departure); ";

				MySqlCommand cmd = new MySqlCommand(sql, con);
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", id_route);
				cmd.Parameters.AddWithValue("@point_of_departure", point_of_departure);
				cmd.Parameters.AddWithValue("@point_of_arrival", point_of_arrival);
				cmd.Parameters.AddWithValue("@time_of_departure", time_of_departure);
				cmd.Parameters.AddWithValue("@time_of_arrival", time_of_arrival);
				cmd.ExecuteNonQuery();
			}
			Console.WriteLine("\n Данные о маршруте поезда были успешно добавленны в базу данных");
		}

		public void UpdateRoute()
		{
			Console.Write("Введите id маршрута, который вы хотите обновить: ");
			int id_route = int.Parse(Console.ReadLine());
			Console.Write("Введите станцию отправления: ");
			string point_of_departure = Console.ReadLine();
			Console.Write("Введите станцию прибытия: ");
			string point_of_arrival = Console.ReadLine();
			Console.Write("Введите время отправления: ");
			DateTime time_of_departure = DateTime.Parse(Console.ReadLine());
			Console.Write("Введите время прибытия: ");
			DateTime time_of_arrival = DateTime.Parse(Console.ReadLine());

			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				string sql = "UPDATE `Route` SET `id_route` = @id_route, `point_of_departure` = @point_of_departure, `point_of_arrival` = @point_of_arrival, `time_of_arrival` = @time_of_arrival, `time_of_departure` = @time_of_departure WHERE `id_route` = @expr;";

				MySqlCommand cmd = new MySqlCommand(sql , con);
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", id_route);
				cmd.Parameters.AddWithValue("@point_of_departure", point_of_departure);
				cmd.Parameters.AddWithValue("@point_of_arrival", point_of_arrival);
				cmd.Parameters.AddWithValue("@time_of_departure", time_of_departure);
				cmd.Parameters.AddWithValue("@time_of_arrival", time_of_arrival);
				cmd.ExecuteNonQuery();
			}
		}

		public void DeleteRoute()
		{
			Console.Write("Введите id маршрута, который вы хотите удалить: ");
			int id_route = int.Parse(Console.ReadLine());

			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				string sql = "DELETE FROM `Route` WHERE `id_route` = @id_route;";

				var cmd = new MySqlCommand(sql, con);
				con.Open();
				cmd.Parameters.AddWithValue("@id_route", id_route);
				cmd.ExecuteNonQuery();
			}

			Console.WriteLine("\n Маршрут был успешно удален");
		}

		public void CallRouteCount()
		{
			Console.Write("Количество маршрутов поездов в таблице: ");

			using (MySqlConnection con = new MySqlConnection(connectionString))
			{
				MySqlCommand cmd = new MySqlCommand("RouteCount", con);
				cmd.CommandType = CommandType.StoredProcedure;
				con.Open();
				MySqlDataReader rdr = cmd.ExecuteReader();
				while (rdr.Read())
				{
					Console.WriteLine(rdr["@count"]);
				}
			}
		}
	}
}

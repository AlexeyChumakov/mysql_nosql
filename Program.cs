﻿using System;

namespace MySQL___NoSQL
{
	class Program
	{
		static void Main()
		{
			TestConnection testConnection = new TestConnection();

			testConnection.GetRouteById();
			Console.WriteLine("---------------");
			testConnection.АddRoute();
			Console.WriteLine("---------------");
			testConnection.GetAllRoutes();
			Console.WriteLine("---------------");
			testConnection.DeleteRoute();
			Console.WriteLine("---------------");
			testConnection.CallRouteCount();
		}
	}
}
